"""create user db

Revision ID: e39e8744a23c
Revises: 
Create Date: 2021-08-02 15:37:42.641553

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e39e8744a23c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "users",
        sa.Column("user_id", sa.Integer, primary_key=True),
        sa.Column("first_name", sa.Text),
        sa.Column("last_name", sa.Text),
        sa.Column("email", sa.Text, unique=True),
        sa.Column("password", sa.Text),
        sa.Column("type", sa.Text),
    )


def downgrade():
    op.drop_table("users")