"""added type to user db

Revision ID: 6f4c4abdbe6b
Revises: e39e8744a23c
Create Date: 2021-08-02 15:43:46.689596

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6f4c4abdbe6b'
down_revision = 'e39e8744a23c'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
