import sys
import re
import random
import math
import json
import httpx
import databases
from pydantic.types import Json
import sqlalchemy.sql
from sqlalchemy import func
from starlette.responses import JSONResponse
from starlette.exceptions import HTTPException
import settings
from models.users import users
import helpers as help

database = databases.Database(settings.DATABASE_URL)

# login funciton for both users and admins
async def login(request):
    """authenticate login by checking email and password

    returns:
        on success: `JWT token` as `str`
        on failure: error message as JSONResponse
    """

    valid_url = re.compile(r"^http://0.0.0.0:8000/users/login[/]?$")
    # captures /login and /login/

    does_url_match = re.match(valid_url, str(request.url))

    if does_url_match:
        print("login Url matchs")
    else:
        raise HTTPException(404)


    # checking if post data is correct
    try:
        post_data = await request.json()
        # print(data)
    except json.JSONDecodeError:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Enter correct JSON data"},
            status_code=400,
        )

    # validate the input and prepare query
    validation_result = await help.validate("LoginData", post_data)

    # returns json if not valid else check if email and admin_id are in admin db
    if isinstance(validation_result, JSONResponse):
        # print("validate() json error called here!")
        return validation_result
    else:
        is_in_db = await help.email_is_in_db(post_data["email"])
        print(f"email in db {is_in_db}")

    # if email not in db, return error, else verify password
    if not is_in_db:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Credentials not found in DB"},
            status_code=404,
        )
    else:
        # verifying password, if correct get type and generate JWT of the correct type
        hash_from_db = await help.get_hash_from_db(post_data["email"])
        post_password = post_data["password"]
        is_correct_password = await help.password_verify(hash_from_db, post_password)
        if isinstance(is_correct_password, JSONResponse):
            return is_correct_password
        elif is_correct_password:
            # else if is_correct_password == True
            # encode jwt with helpers
            is_user_admin = await help.is_admin(post_data["email"])
            jwt_token = await help.jwt_encode(post_data, is_user_admin)
            return JSONResponse(
                {"status": "SUCCESS", "msg": "Login successful", "result": jwt_token}
            )


# list users with pagination, default limit=5
async def list_all(request):
    print(f"list_all(): url = {request.url}")
    # print(f"list_all(): url path = {request.url.path}")-> /users/

    no_args_url = re.compile(r"^http://0.0.0.0:8000/users[/]?$")
    # captures /users and /users/

    paged_url = re.compile(r"^http://0.0.0.0:8000/users[/]?\?page=\w*$")
    # captures /users?page= and /users/?page=

    # getting & checking the query param - page
    if no_args_url.match(str(request.url)):
        print("list_all(): No args in url.")
        page = 1
    elif paged_url.match(str(request.url)):
        try:
            page = int(request.query_params["page"])
            print(page)
        except ValueError:
            return JSONResponse(
                content={"status": "ERROR", "msg": "Invalid input for page number"},
                status_code=400,
            )
    else:
        raise HTTPException(404)

    # getting the total number of items in DB
    count = await help.get_user_count()
    if not isinstance(count, int):
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "Error in getting total no of items in DB",
            },
            status_code=500,
        )

    # set total pages and offset
    limit = 5
    total_pages = math.ceil(count / limit)
    if page == 0:
        page = 1
        offset = 0
    elif page > total_pages:
        offset = (total_pages - 1) * limit
    else:
        offset = (page - 1) * limit
    query = users.select().limit(limit).offset(offset)

    # check if query executed correctly and prepare the result as josn
    try:
        results = await database.fetch_all(query)
    except:
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "Error occured while running query in DB",
            },
            status_code=500,
        )
    else:
        result_dict = [
            {
                "user_id": result["user_id"],
                "first_name": result["first_name"],
                "last_name": result["last_name"],
                "email": result["email"],
                "type": result["type"],
            }
            for result in results
        ]

    if page > total_pages:
        content = {
            "status": "SUCCESS",
            "msg": "Listing user details",
            "total_result": count,
            "current_page": "Page limit exceeded! Displaying Last Page.",
            "total_page": total_pages,
            "result": result_dict,
        }
    else:
        content = {
            "status": "SUCCESS",
            "msg": "Listing user details",
            "total_result": count,
            "current_page": page,
            "total_page": total_pages,
            "result": result_dict,
        }
    return JSONResponse(content)


# list specific user
async def list_target(request):
    # target_id_url = re.compile(r"^http://0.0.0.0:8080/user/\d{1,4}$")
    # captures /user/213

    if request.query_params:
        print("params present")
        raise HTTPException(404)

    # get the target_id from request
    try:
        target_id = int(request.path_params["target_id"])
        # raises ValueError if target_id is not of type int
        print(f"list_taget()= {target_id}")
    except ValueError:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Invalid input for target_id"},
            status_code=400,
        )
    else:
        is_in_db = await help.user_is_in_db(target_id)

    if is_in_db:
        try:
            query = users.select().where(users.c.user_id == target_id)
            result = await database.fetch_one(query)
        except:
            return JSONResponse(
                content={
                    "status": "ERROR",
                    "msg": "Error occured while running query in DB",
                },
                status_code=500,
            )
        else:
            print(f"list_target(): Result of finding the ID in DB: {result}")
            # print(result[0])
            # print(type(result))
            # print(type(result[0]))
            return JSONResponse(
                {
                    "status": "SUCCESS",
                    "msg": f"Displaying details of emp: {target_id}",
                    "result": {
                        "user_id": result[0],
                        "first_name": result[1],
                        "last_name": result[2],
                        "email": result[3],
                        "type": result[5],
                    },
                }
            )
    else:
        return JSONResponse(
            content={"status": "ERROR", "msg": "User not found in DB"},
            status_code=404,
        )


# create user with json input
async def create_user(request):
    print(f"Create(): url = {request.url}")
    # checking if input json data is correct
    try:
        post_data = await request.json()
        # print(data)
    except json.JSONDecodeError:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Enter correct JSON data"},
            status_code=400,
        )

    # pydanctic validate the input and prepare query
    validation_result = await help.validate("CreateUser", post_data)

    # returns json if not valid else prepares the query
    if isinstance(validation_result, JSONResponse):
        # print("validate() json error called here!")
        return validation_result
    else:
        hashed_password = await help.password_hasher(post_data["password"])
        query = users.insert().values(
            first_name=post_data["first_name"],
            last_name=post_data["last_name"],
            email=post_data["email"],
            password=hashed_password,
            type=post_data["type"],
        )

    # running the query
    try:
        result = await database.execute(query)
    except:
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "Error occured while running query in DB",
            },
            status_code=500,
        )
    else:
        # insert details of created user into as plaintext in users.txt
        created_user = {
            "user_id": result,
            "email": post_data["email"],
            "password": post_data["password"],
            "type": post_data["type"],
        }
        with open("users.txt", "a") as user_file:
            print(created_user, file=user_file)

        return JSONResponse(
            {
                "status": "SUCCESS",
                "msg": "User created correctly.",
                "result": {
                    "user_id": result,
                    "first_name": post_data["first_name"],
                    "last_name": post_data["last_name"],
                    "email": post_data["email"],
                    "password": post_data["password"],
                    "type": post_data["type"],
                },
            }
        )


# create random user automatically
async def create_random(request):
    # fetching data from RandomUserGen API & preparing query
    async with httpx.AsyncClient() as client:
        response = await client.get("https://randomuser.me/api/?results=1")
    if response.status_code != 200:
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "RandomUserApi is not online right now!",
            },
            status_code=404,
        )
    else:
        print(f"create_random(): Response from RandomUserAPI= {response}")
        res_data = response.json()
        pw_from_getdata = res_data["results"][0]["login"]["username"]
        # print(f"pw_from_getdata={pw_from_getdata}")
        hashed_password = await help.password_hasher(pw_from_getdata)
        query = users.insert().values(
            first_name=res_data["results"][0]["name"]["first"],
            last_name=res_data["results"][0]["name"]["last"],
            email=res_data["results"][0]["email"],
            password=str(hashed_password),
            type="employee",
        )

    # executing query
    try:
        result = await database.execute(query)
    except:
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "Error occured while running query in DB",
            },
            status_code=500,
        )
    else:
        # insert details of created user into as plaintext in users.txt
        created_user = {
            "user_id": result,
            "email": res_data["results"][0]["email"],
            "password": res_data["results"][0]["login"]["username"],
            "type": "employee",
        }
        with open("users.txt", "a") as user_file:
            print(created_user, file=user_file)

        print(f"create_random(): result = {result}")
        return JSONResponse(
            {
                "status": "SUCCESS",
                "msg": "Random Employee created",
                "result": {
                    "user_id": result,
                    "first_name": res_data["results"][0]["name"]["first"],
                    "last_name": res_data["results"][0]["name"]["last"],
                    "email": res_data["results"][0]["email"],
                    "password": res_data["results"][0]["login"]["username"],
                    "type": "employee",
                },
            }
        )
