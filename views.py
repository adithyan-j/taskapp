from starlette.responses import HTMLResponse, PlainTextResponse


async def home(request):
    return HTMLResponse("HomePage")


async def database(request):
    return HTMLResponse("Databases have been moved to /list")
