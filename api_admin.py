import re
import json
import math
import databases
from sqlalchemy import func
from starlette.responses import JSONResponse
from starlette.config import Config
import settings
from models.users import users
import helpers as help

config = Config(".env")
database = databases.Database(settings.DATABASE_URL)

# update target employee with json input
async def update_user(request):
    # get jwt_token and validate it schema
    token_header = request.headers.get("authorization", None)
    if token_header is None:
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "Token not found. First Login and generate token.",
            },
            status_code=400,
        )
    jwt_token = str(token_header).split(" ")[1]
    print(f"token received = {jwt_token}")

    # decode the token, check for error
    decoded_token = await help.jwt_decode(jwt_token)
    if isinstance(decoded_token, JSONResponse):
        return decoded_token

    # check if decoded token follows the described schema, on success check if its an admin token
    schema_validation_result = await help.validate("JWTToken", decoded_token)
    if isinstance(schema_validation_result, JSONResponse):
        return schema_validation_result
    else:
        is_user_admin = decoded_token["admin"]
        if not is_user_admin:
            return JSONResponse(
                content={
                    "status": "ERROR",
                    "msg": "User is not admin, Only Admins can Update user details.",
                },
                status_code=401,
            )

    # if everything above passes then do the db operation
    # check qeury param target_id
    try:
        target_id = int(request.path_params["target_id"])
        print(f"target_id received = {target_id}")

    except ValueError:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Invalid input for target_id"},
            status_code=400,
        )
    else:
        is_in_db = await help.user_is_in_db(target_id)
        # print(f"in DB: {is_in_db}")

    if is_in_db:
        # check json input data is correct
        try:
            post_data = await request.json()
            print(f"post_data received = {post_data}")
            if not post_data:
                return JSONResponse(
                    content={"status": "ERROR", "msg": "Empty request body"},
                    status_code=400,
                )
        except json.JSONDecodeError:
            return JSONResponse(
                content={"status": "ERROR", "msg": "Enter JSON data correctly"},
                status_code=400,
            )

        # validate the input and prepare query
        # returns json if not valid else prepares the query
        validation_result = await help.validate("UpdateUser", post_data)
        if isinstance(validation_result, JSONResponse):
            return validation_result
        else:
            # logic for when the admin doesn't have to update all the fields of an employee
            # prepare temp placeholders for all the fields of employee
            # select all the current fields of target and fill them in their respective temp placeholders
            query = users.select().where(users.c.user_id == target_id)
            row = await database.fetch_one(query)
            temp_first = row[1]
            temp_last = row[2]
            temp_email = row[3]
            temp_type = row[5]

            # identify the fields present in post_data(data the admin wants to update)
            # update temp placeholders of those fields with the post_data
            for item in post_data:
                if item == "first_name":
                    temp_first = post_data["first_name"]
                elif item == "last_name":
                    temp_last = post_data["last_name"]
                elif item == "email":
                    temp_email = post_data["email"]
                elif item == "type":
                    temp_type = post_data["type"]

            # prepare query with temp placeholder data which have both the current values
            # and the new employee data from post_data
            query = (
                users.update()
                .where(users.c.user_id == target_id)
                .values(
                    user_id=target_id,
                    first_name=temp_first,
                    last_name=temp_last,
                    email=temp_email,
                    type=temp_type,
                )
            )

        # executing the query
        try:
            result = await database.execute(query)
            print(f"update(): Result= {result}")
        except Exception as e:
            return JSONResponse(
                content={
                    "status": "ERROR",
                    "msg": str(e),
                },
                status_code=500,
            )
        else:
            if result == 1:
                return JSONResponse(
                    {
                        "status": "SUCCESS",
                        "message": "Employee details updated successfully.",
                        "result": {
                            "user_id": target_id,
                            "first_name": temp_first,
                            "last_name": temp_last,
                            "email": temp_email,
                            "type": temp_type,
                        },
                    }
                )
            else:
                return JSONResponse(
                    content={
                        "status": "ERROR",
                        "msg": "Error occured while running query in DB",
                    },
                    status_code=500,
                )
    else:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Employee not found in DB"},
            status_code=404,
        )


# delete the target employee
async def delete_user(request):
    # get jwt_token and validate it schema
    token_header = request.headers.get("authorization", None)
    if token_header is None:
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "Token not found. First Login and generate token.",
            },
            status_code=400,
        )
    jwt_token = str(token_header).split(" ")[1]
    print(f"token received = {jwt_token}")

    # decode the token, check for error
    decoded_token = await help.jwt_decode(jwt_token)
    if isinstance(decoded_token, JSONResponse):
        return decoded_token

    # see if decoded token follows the described schema, on success check if its an admin token
    schema_validation_result = await help.validate("JWTToken", decoded_token)
    if isinstance(schema_validation_result, JSONResponse):
        return schema_validation_result
    else:
        is_user_admin = decoded_token["admin"]
        if not is_user_admin:
            return JSONResponse(
                content={
                    "status": "ERROR",
                    "msg": "User is not admin, Only Admins can Delete user details.",
                },
                status_code=401,
            )

    # if everything above passes then do the db operation
    # check qeury param target_id
    try:
        target_id = int(request.path_params["target_id"])
    except ValueError:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Invalid input for target_id"},
            status_code=400,
        )
    else:
        is_in_db = await help.user_is_in_db(target_id)

    if is_in_db:
        try:
            # check if user wants to delete his own account
            query = (
                users.select()
                .with_only_columns([users.c.email])
                .where(users.c.user_id == target_id)
            )
            result = await database.fetch_one(query)
            target_email = result[0]
            decoded_email = decoded_token["sub"]
            print(f"target_enail = {target_email}")
            print(f"decoded sub= {decoded_email}")

            if target_email == decoded_token["sub"]:
                return JSONResponse(
                    content={
                        "status": "ERROR",
                        "msg": "You cannot delete your own account.",
                    },
                    status_code=400,
                )
        except Exception as e:
            return JSONResponse(
                content={"status": "ERROR", "msg": str(e)},
                status_code=400,
            )

        try:
            query = users.delete().where(users.c.user_id == target_id)
            result = await database.execute(query)
            # print(f"delete(): type of result= {type(result)}")
        except:
            return JSONResponse(
                content={
                    "status": "ERROR",
                    "msg": "Error occured while running query in DB",
                },
                status_code=500,
            )
        else:
            if result == 1:
                return JSONResponse(
                    content={
                        "status": "SUCCESS",
                        "msg": f"Employee {target_id} deleted",
                    },
                    status_code=200,
                )
    else:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Employee not found in DB"},
            status_code=404,
        )


# region old functions:

# region helper function to list all the admins
# async def list_all_admins(request):
#     print(f"list_all(): url = {request.url}")
#     # print(f"list_all(): url path = {request.url.path}")-> /admin/

#     no_args_url = re.compile(r"^http://0.0.0.0:8000/admin[/]?$")
#     # captures /admin and /admin/

#     paged_url = re.compile(r"^http://0.0.0.0:8000/admin[/]?\?page=\w*$")
#     # captures /admin?page= and /admin/?page=

#     # getting & checking the query param - page
#     if no_args_url.match(str(request.url)):
#         print("list_all(): No args in url.")
#         page = 1
#     elif paged_url.match(str(request.url)):
#         try:
#             page = int(request.query_params["page"])
#             print(page)
#         except ValueError:
#             return JSONResponse(
#                 content={"status": "ERROR", "msg": "Invalid input for page number"},
#                 status_code=400,
#             )
#     else:
#         raise HTTPException(404)

#     # getting the total number of items in DB
#     count = await help.get_admin_count()
#     if not isinstance(count, int):
#         return JSONResponse(
#             content={
#                 "status": "ERROR",
#                 "msg": "Error in getting total no of items in DB",
#             },
#             status_code=500,
#         )

#     # set total pages and offset
#     limit = 5
#     total_pages = math.ceil(count / limit)
#     if page == 0:
#         page = 1
#         offset = 0
#     elif page > total_pages:
#         offset = (total_pages - 1) * limit
#     else:
#         offset = (page - 1) * limit
#     query = admin.select().limit(limit).offset(offset)

#     # check if query executed correctly and prepare the result as josn
#     try:
#         results = await database.fetch_all(query)
#     except:
#         return JSONResponse(
#             content={
#                 "status": "ERROR",
#                 "msg": "Error occured while running query in DB",
#             },
#             status_code=500,
#         )
#     else:
#         result_dict = [
#             {
#                 "admin_id": result["admin_id"],
#                 "first_name": result["first_name"],
#                 "last_name": result["last_name"],
#                 "email": result["email"],
#             }
#             for result in results
#         ]

#     if page > total_pages:
#         content = {
#             "status": "SUCCESS",
#             "msg": "Listing admin details",
#             "total_result": count,
#             "current_page": "Page limit exceeded! Displaying Last Page.",
#             "total_page": total_pages,
#             "result": result_dict,
#         }
#     else:
#         content = {
#             "status": "SUCCESS",
#             "msg": "Listing admin details",
#             "total_result": count,
#             "current_page": page,
#             "total_page": total_pages,
#             "result": result_dict,
#         }
#     return JSONResponse(content)

# endregion list admins

# region admin_login to generate jwt token
# async def admin_login(request):
#     """
#     authenticate admin by checking email and admin_id
#     returns:
#         on success: `JWT token` as `str`
#         on failure: error message as JSONResponse
#     """
#     # checking if post data is correct
#     try:
#         post_data = await request.json()
#         # print(data)
#     except json.JSONDecodeError:
#         return JSONResponse(
#             content={"status": "ERROR", "msg": "Enter correct JSON data"},
#             status_code=400,
#         )

#     # validate the input and prepare query
#     validation_result = await help.validate("LoginData", post_data)

#     # returns json if not valid else check if email and admin_id are in admin db
#     if isinstance(validation_result, JSONResponse):
#         print("validate() json error called here!")
#         return validation_result
#     else:
#         is_in_db = await help.admin_is_in_db(post_data)
#         print(f"admin in db {is_in_db}")

#     # if admin not in admin db, return error, else prepare payload
#     if not is_in_db:
#         return JSONResponse(
#             content={"status": "ERROR", "msg": "Credentials not found in Admin DB"},
#             status_code=404,
#         )
#     else:
#         # encode jwt with helpers
#         jwt_token = await help.jwt_encode(post_data)

#     return JSONResponse(
#         {"status": "SUCCESS", "msg": "Login successful", "result": jwt_token}
#     )
# endregion admin_login

# endregion old functions
