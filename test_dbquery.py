from sqlalchemy import create_engine
from sqlalchemy import func, and_
from sqlalchemy import sql
import databases
import settings
import asyncio
from models.admin import admin
from models.employee import emp
from models.users import users

engine = create_engine("sqlite:///test.db")
conn = engine.connect()

database = databases.Database(settings.DATABASE_URL)

with engine.connect() as conn:
    # query = users.insert().values(
    #     first_name="first",
    #     last_name="last",
    #     email="email3@email.com",
    #     password="results21312username",
    #     type="employee"
    # )
    # result = conn.execute(query)
    target_email = "last@employee.com"
    query = (
        users.select()
        .with_only_columns([users.c.type])
        .where(users.c.email == "adminone@admin.com")
    )
    result = conn.execute(query)
    result = result.fetchone()
    print(f"fetchone result={result[0]}")
    # print(result[0])

    # for item in result:
    #     print(item)
    # s = emp.select()
    # count_query = func.count(emp.c.emp_id)
    # email = "firstadmin@admin.com"
    # admin_id = 300
    # query = admin.filter(admin.c.first_name).where(admin.c.email==email and admin.c.admin_id==admin_id)
    # query = admin.select().where(
    #     and_(admin.c.email == email, admin.c.admin_id == admin_id)
    # )

    # query = emp.select().where(emp.c.emp_id == 102)
    # for item in temp:
    # print(query)
    # count = conn.execute(query)
    # print("coutn:")
    # print(count)
    # print(count.fetchone()[0])
    # for item in count:
    #     print(item)
    # print(count.fetchone()[0])
    # query = emp.select().limit(10).offset(10)
    # rs = conn.execute(query)
    # for row in rs:
    #     print(row)
