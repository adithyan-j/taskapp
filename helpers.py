from enum import Enum
from typing import Optional
import databases
import jwt
import datetime
from argon2 import PasswordHasher
from argon2.exceptions import HashingError, InvalidHash, VerifyMismatchError
from pydantic import BaseModel, ValidationError, EmailStr
from pydantic.types import StrictInt, StrictBool, StrictStr
from sqlalchemy import select, func
from sqlalchemy.sql.expression import true, and_
from sqlalchemy.sql.functions import ReturnTypeFromArgs
from starlette.responses import JSONResponse
from starlette.config import Config
import settings
from models.users import users
from models.employee import emp
from models.admin import admin

database = databases.Database(settings.DATABASE_URL)
config = Config(".env")

# gets emps total count, JSON response on error
async def get_user_count():
    # print(emp.select([func.count()]).select_from(emp))
    # print(emp.select([func.count()]))
    query = users.select(users.c.user_id).count()
    try:
        result = await database.fetch_one(query)
    except:
        return JSONResponse({"error": "Error in executing get_count_in_db."})
    else:
        return result[0]


# checks if user is in db
async def user_is_in_db(target_id):
    # prepare query
    query = users.select().where(users.c.user_id == target_id)
    try:
        result = await database.fetch_one(query)
    except:
        return JSONResponse({"error": "Error in executing user_is_in_db."})
    print(f"type  = {type(result)}")
    if result is None:
        return False
    else:
        print(f"user_is_in_db(): result  = {result}")
        return True


# checks if target email is in db
async def email_is_in_db(target_email):
    """Searchs for email in db

    params:
        email: type: str

    returns:
    bool: True or False based on success of the query
    """

    # prepare query
    query = users.select().where(users.c.email == target_email)
    try:
        result = await database.fetch_one(query)
    except:
        return JSONResponse({"error": "Error in executing email_is_in_db."})
    print(f"type  = {type(result)}")
    if result is None:
        return False
    else:
        print(f"user_is_in_db(): result  = {result}")
        return True


# gets password hash of target user from db
async def get_hash_from_db(target_email):
    """Returns hash of the from db

    params:
        email: email of the target user
            type: str

    returns:
        hash: hash of target users password from db
            type: str
    """
    query = (
        users.select()
        .with_only_columns([users.c.password])
        .where(users.c.email == target_email)
    )
    try:
        result = await database.fetch_one(query)
        hash = result[0]
    except:
        return JSONResponse({"error": "Error in executing get_hash_from_db."})
    else:
        print(f"hash from db  = {hash}")
        return hash


# checks if target user is admin
async def is_admin(target_email):
    """Returns type of the from db

    params:
        email: email of the target user
            type: str

    returns:
        result: `True` if target user is admin else `False`
    """
    query = (
        users.select()
        .with_only_columns([users.c.type])
        .where(users.c.email == target_email)
    )
    try:
        result = await database.fetch_one(query)
        type = result[0]
    except:
        return JSONResponse({"error": "Error in executing get_type_from db."})
    else:
        print(f"type from db  = {type}")
        if type == "admin":
            return True
        else:
            return False


# region JWT encode and decode functions
async def jwt_encode(post_data, is_user_admin):
    """Uses jwt.encode(payload,secret,alogrithm) of PyJWT to encode data

    params:
        post_data: post_data to fetch email and user_id
            type: dict (of class type `JWTToken` defined in `helpers.py`)

        is_user_admin: indicates if the user is admin or not
            type: bool
    """
    payload = {
        "sub": post_data["email"],
        "admin": is_user_admin,
        # token valid for 5 minute
        "exp": datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=300),
        "iat": datetime.datetime.utcnow(),
    }
    try:
        jwt_token = jwt.encode(payload, config("SECRET"), algorithm="HS256")
    except Exception as e:
        return JSONResponse({"status": "ERROR", "msg": str(e)})
    else:
        return jwt_token


async def jwt_decode(input_token):
    # print(f"Input token = {input_token}")
    try:
        decoded_jwt_token = jwt.decode(
            input_token, str(config("SECRET")), algorithms=["HS256"]
        )
    except jwt.ExpiredSignatureError:
        print("Expired Token Error")
        return JSONResponse(
            content={"status": "ERROR", "msg": "Token expired. Login again."},
            status_code=408,
        )
    except jwt.InvalidTokenError:
        print("Invalid Token Error")
        return JSONResponse(
            content={"status": "ERROR", "msg": "Invalid token received"},
            status_code=400,
        )
    else:
        print(f"decoded token = {decoded_jwt_token}")
        return decoded_jwt_token


# endregion jwt token


# region Password hashing and decoding
async def password_hasher(input_password):
    """Hashes plaintext with hash() of argon2 lib

    params:
    input_password: a plaintext string
        type: str

    returns:
    hash: argon2 hash
        type: unicode str
    """
    try:
        ph = PasswordHasher()
        hash = ph.hash(bytes(input_password, "utf-8"))
        print(f"hash= {hash}")
    except HashingError as e:
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "Error occured while hashing Password",
                "result": str(e.args[0]),
            },
            status_code=500,
        )
    else:
        return hash


async def password_verify(hash, input_password):
    """verifies plaintext with verify() of argon2 lib

    params:
    hash: hash from the db
        type: argon2 hash
    input_password: a plaintext string
        type: str

    returns:
    on succcess: `True`
    on failure: exceptions as JSONresposnse

    """
    try:
        ph = PasswordHasher()
        result = ph.verify(hash, bytes(input_password, "utf-8"))
    except VerifyMismatchError:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Input password is incorrect"}
        )
    except InvalidHash:
        return JSONResponse(
            content={"status": "ERROR", "msg": "Hash from DB was invalid"},
            status_code=404,
        )
    except:
        return JSONResponse(
            content={
                "status": "ERROR",
                "msg": "Error occured while Verifying Password",
            },
            status_code=404,
        )
    else:
        return result


# endregion password


# region Pydantic data classes and validations
class UserType(str, Enum):
    employee = "employee"
    admin = "admin"


class CreateUser(BaseModel):
    first_name: StrictStr
    last_name: StrictStr
    email: EmailStr
    password: StrictStr
    type: UserType

    class Config:
        extra = "forbid"


class UpdateUser(BaseModel):
    first_name: Optional[StrictStr]
    last_name: Optional[StrictStr]
    email: Optional[EmailStr]

    class Config:
        extra = "forbid"


class LoginData(BaseModel):
    email: EmailStr
    password: StrictStr

    class Config:
        extra = "forbid"


class JWTToken(BaseModel):
    sub: EmailStr
    admin: StrictBool
    exp: StrictInt
    iat: StrictInt

    class Config:
        extra = "forbid"


async def validate(data_class, input_data):
    """Validates the input against defined Pydantic data classes defined in `helpers.py`

    params:
        data_class : values: "CreateUser" | "UpdateUser" | "LoginData" | "JWTToken"
        type : str

    input_data:  dict with schema based on pydantic class types defined in `helpers.py`

    """
    try:
        if data_class == "CreateUser":
            # is_data_vaild = Input.parse_raw(json.dumps(data))
            is_data_vaild = CreateUser(**input_data)
        elif data_class == "LoginData":
            is_data_vaild = LoginData(**input_data)
        elif data_class == "UpdateUser":
            is_data_vaild = UpdateUser(**input_data)
        elif data_class == "JWTToken":
            is_data_vaild = JWTToken(**input_data)
    except ValidationError as e:
        return JSONResponse(
            content={
                "status": "ERROR",
                "message": f"{len(e.errors())} Errors found",
                "error_list": [
                    {
                        "location": err["loc"][0],
                        "msg": err["msg"],
                        "error_type": err["type"],
                    }
                    for err in e.errors()
                ],
            },
            status_code=400,
        )


# endregion pydantic


# region old functions:

# async def emp_is_in_db(target_id):
#     query = emp.select(emp.c.emp_id).where(emp.c.emp_id == target_id)
#     try:
#         result = await database.fetch_one(query)
#     except:
#         return JSONResponse({"error": "Error in executing count query in DB."})
#     print(f"type  = {type(result)}")
#     if result is None:
#         return False
#     else:
#         print(f"emp_is_in_db(): result  = {result}")
#         return True


# endregion
