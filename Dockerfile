# syntax=docker/dockerfile:1

FROM python:3.9.6-slim-buster
EXPOSE 8000
WORKDIR /app
COPY requirements.txt requirements.txt
COPY . .
RUN pip install -r requirements.txt
CMD ["python3", "app.py"]