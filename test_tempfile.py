from pydantic.networks import EmailStr
from sqlalchemy import func, and_
import sqlalchemy
import databases
import re
import json
from enum import Enum
from pydantic import BaseModel, StrictStr, EmailStr
from argon2 import PasswordHasher
from starlette.responses import JSONResponse
from models.admin import admin
from models.employee import emp


created_user = {
    "emp_id": 111,
    "email": "email",
    "password": "passwords234",
    "type": "employee",
}

# with open("users.txt", "a") as user_file:
#    print(created_user, file=user_file)

# region testing argon2 hashing
# hash = "$argon2id$v=19$m=102400,t=2,p=8$ODhVpt7CfcXsRcBcvq1QSA$m6zupm1yrQ0xM+0TruAJ8Q"
# password = "adminone"
# pw = PasswordHasher()
# result = pw.hash(bytes(password, "utf-8"))
# result = pw.verify(hash, bytes(password, 'utf-8'))
# print(f"answer = {result}")
# print(f"answer = {str(result)}")
# if result==str(result):
#    print("same")
# else:
#    print("not same")
# endregion


# region Pydantic
# class UserType(str, Enum):
#    employee= "employee"
#    admin= "admin"

# class CreateUser(BaseModel):
#    name: StrictStr
#    type: UserType


# test = {"name": "first", "car":"bmw"}
# try:
#    result=  CreateUser(**test)
# except Exception as e:
#    print(e)
# endregion


# asnwer = None

# if asnwer:
#     print("inside if")
# else:
#     print("inside else")

asnwer = re.match("^http://0.0.0.0:8000/users/login[/]?$", "http://0.0.0.0:8000/users/login") 
print(asnwer)