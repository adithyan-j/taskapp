import uvicorn
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse

# from routes import all_routes
import routes
import settings


exception_handlers = {
    400: routes.bad_request,
    404: routes.not_found,
    405: routes.not_allowed,
}

app = Starlette(
    debug=True, routes=routes.all_routes, exception_handlers=exception_handlers
)


if __name__ == "__main__":
    uvicorn.run(
        app,
        debug=True,
        host="0.0.0.0",
        port=8000,
        # port=8080,
    )


# async def app(scope, receive, send):
#     assert scope['type'] == 'http'
#     response = PlainTextResponse('Hello, world!')
#     await response(scope, receive, send)
