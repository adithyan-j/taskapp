from enum import unique
import sqlalchemy

metadata = sqlalchemy.MetaData()

emp = sqlalchemy.Table(
    "employee",
    metadata,
    sqlalchemy.Column("emp_id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("first_name", sqlalchemy.String(100)),
    sqlalchemy.Column("last_name", sqlalchemy.String(100)),
    sqlalchemy.Column("email", sqlalchemy.String(100), unique=True),
)
