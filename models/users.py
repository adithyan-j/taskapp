import sqlalchemy

metadata = sqlalchemy.MetaData()

users = sqlalchemy.Table(
    "users",
    metadata,
    sqlalchemy.Column("user_id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("first_name", sqlalchemy.Text),
    sqlalchemy.Column("last_name", sqlalchemy.Text),
    sqlalchemy.Column("email", sqlalchemy.Text, unique=True),
    sqlalchemy.Column("password", sqlalchemy.Text),
    sqlalchemy.Column("type", sqlalchemy.Text),
)
