from starlette.routing import Route
from starlette.responses import JSONResponse
import api_employees as apiemp
import api_admin as apiadmin
import views

all_routes = [
    Route("/", views.home, name="home"),
    Route("/database", views.database, methods=["GET"]),
    Route("/users", apiemp.list_all, methods=["GET"]),
    Route("/users/", apiemp.list_all, methods=["GET"]),
    Route("/users", apiemp.create_user, methods=["POST"]),
    Route("/users/", apiemp.create_user, methods=["POST"]),
    Route("/users/random", apiemp.create_random, methods=["POST"]),
    Route("/users/random/", apiemp.create_random, methods=["POST"]),
    Route("/users/login", apiemp.login, methods=["GET"]),
    Route("/users/login/", apiemp.login, methods=["GET"]),
    Route("/users/{target_id}", apiemp.list_target, methods=["GET"]),
    Route("/users/{target_id}", apiadmin.update_user, methods=["PUT"]),
    Route("/users/{target_id}", apiadmin.delete_user, methods=["DELETE"]),
    # Route("/admin", apiadmin.list_all_admins, methods=["GET"]),
    # Route("/admin/", apiadmin.list_all_admins, methods=["GET"]),
]


async def bad_request(request, exc):
    return JSONResponse(
        content={"status": "400 ERROR", "msg": "Bad request invalid input or URL"},
        status_code=400,
    )


async def not_found(request, exc):
    return JSONResponse(
        content={"status": "404 ERROR", "msg": "Not found, Invalid URL"},
        status_code=404,
    )


async def not_allowed(request, exc):
    return JSONResponse(
        content={"status": "405 ERROR", "msg": "HTTP method is not allowed"},
        status_code=405,
    )
