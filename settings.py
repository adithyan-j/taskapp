from pathlib import Path
from starlette.config import Config

config = Config(".env")

DATABASE_URL = config("DATABASE_URL")

DEBUG = config("DEBUG", cast=bool, default=True)

BASE_DIR = Path(__file__).parent

TEMPLATES_DIR = BASE_DIR / "templates"
