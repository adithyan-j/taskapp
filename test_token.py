import jwt
import datetime
from starlette.config import Config

config = Config(".env")

secret = "S0ME_random_$7r1ng"

header = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlbWFpbEBlYW1pbC5jb20iLCJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjI3OTgyOTExLCJpYXQiOjE2Mjc5ODI2MTF9.fyGgl0ZAoGBQO_m3fCW22zuoRUHBg13J3a8764PB7C8"

token = str(header).split(" ")[1]
print(token)

try:
    decoded_jwt_token = jwt.decode(token, config("SECRET"), algorithms=["HS256"])
    email = decoded_jwt_token["sub"]
    ty = type(decoded_jwt_token["admin"])
    print(f"email = {ty}")
except jwt.ExpiredSignatureError:
    print("EXPIRED TOKEN")
except jwt.InvalidTokenError:
    print("INVALID TOKEN")
else:
    print(decoded_jwt_token)

# payload = {
#     "sub": "email@eamil.com",
#     "admin": True,
#     # token valid for 5 minute
#     "exp": datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=300),
#     "iat": datetime.datetime.utcnow(),
# }
# try:
#     jwt_token = jwt.encode(payload, config("SECRET"), algorithm="HS256")
# except Exception as e:
#     print(str(e))
# else:
#     print(f"token={jwt_token}")
